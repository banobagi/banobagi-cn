$(document).ready(function () {
    siteMap.init();

});



var siteMap = {
    init: function(){
        siteMap.toggleMap();
        siteMap.resetMap();
		siteMap.loadMap();
    },
    toggleMap: function () {
        $('.js-open-site').on('click',function (e) {
            e.preventDefault();
            var winSize = $(window).innerWidth(),
                self = $(this);
            if(winSize < 681){
                self.toggleClass('is-open').siblings('.guidance__category--2nd').stop().slideToggle(250);
            }
        });
    },
    resetMap: function () {
        $(window).on('resize load',function () {
            var winSize = $(window).innerWidth();
            if(winSize > 681){
                $('.guidance__category--1th').removeClass('is-open');
                $('.guidance__category--2nd').show();
				$('.guidance').addClass('is-on');
            }else{
				if($('.guidance').hasClass('is-on')){
					siteMap.moShow();
				}
            }
        });
	},
	loadMap: function () {
        $(window).on('load',function () {
            var winSize = $(window).innerWidth();
            if(winSize < 681) siteMap.moShow();
        });
	},
	moShow: function () {
		$('.guidance__category--2nd').hide();
		$('.guidance__category--1th').removeClass('is-open');
		$('.guidance__category--1th').eq(0).addClass('is-open').siblings('.guidance__category--2nd').stop().show();
		$('.guidance').attr('data-mobile',false);
		$('.guidance').removeClass('is-on');
	}
};

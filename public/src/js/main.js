$(document).ready(function(){
	$(window).on("load resize", function () {
		var winH = $(window).height();
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 1024) ? true : false;
				return r;
			}())
		);

		$('body').attr('only-mobile',
			(function(){
				var r = ($(window).width() <= 680) ? true : false;
				return r;
			}())
		);
		if ($('body').attr('data-mobile') == 'true'){
			$('.detail-thumbnail__link').off('mouseenter');
		}
		$('.intro').height(winH);
	});//load,resize
	$(window).on("load", function () {
		$('.detail-thumbnail__item').eq(1).addClass('is-active');
		$('.js-main-txt1').animate({'opacity':'1'},2000,function(){
            $('.js-main-txt2').animate({'opacity':'1'},2000,function(){
                var typed = new Typed('#typed', {
                    stringsElement: '#typed-strings',
                    typeSpeed: 100,
                    backSpeed: 0,
                    startDelay: 0,
                    loop: false,
                    loopCount: Infinity,
                    onStringTyped:function(){
                        $('.js-main-txt3').animate({'opacity':'1'},2000,function(){
                            $('.typed-cursor').removeClass('is-focus').delay(500).animate({'opacity':'0'},1400);
                        });
                    }
                });
                $('.typed-cursor, .intro-inner__tit').css('opacity','1');
                $('.typed-cursor').addClass('is-focus');
            });
        });
	});
	$(window).on("scroll", function(){
		var introVidH = $(".intro-video").height();

		if($(window).scrollTop() > introVidH){
			videoStop();
		}else{
			videoPlay();
		}
	});
});

//영상플레이
function videoPlay() {
    f = $(".intro-video__iframe");

    if (!f || !f.attr("src")) return;

    url = f.attr("src").split("?")[0];

    var data = {
        "method" : "play",
        "value" : 1
    };
    f[0].contentWindow.postMessage(JSON.stringify(data), url);
}
//영상 일시정지
function videoStop() {
    f = $(".intro-video__iframe");
    if (!f || !f.attr("src")) return;

    url = f.attr("src").split("?")[0];

    var data = {
        "method" : "pause",
        "value" : 1
    };
    f[0].contentWindow.postMessage(JSON.stringify(data), url);
}


//메인 검색
$('#main_keyword').focus(function() {
	$(this).parents('.search-keyword').find('.search-keyword__btn').addClass('is-focus');
})
.blur(function() {
	$(this).parents('.search-keyword').find('.search-keyword__btn').removeClass('is-focus');
});

//리얼스토리
$('.real__link').on('mouseenter',function(){
	$(this).closest('.real__item').addClass('is-active');
}).on('mouseleave',function(){
	$(this).closest('.real__item').removeClass('is-active');
});

//디테일 슬라이드
$('.js-detail-slide').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 4000,
	zIndex:10,
	dotsClass:'detail-slide__dot slide-dot'
});

//오시는길 문자보내기팝업
$('.js-sms-btn').on('click',function(e){
	e.preventDefault();
	$('.main-form').addClass('is-active');
});
$('.js-sms-close').on('click',function(e){
	e.preventDefault();
	$('.main-form').removeClass('is-active');
});


//디테일리스트 썸네일 오버효과
$('.detail-thumbnail__link').on('mouseenter',function(){
	$(this).closest('.detail-thumbnail__item').addClass('is-hover');
}).on('mouseleave',function(){
	$(this).closest('.detail-thumbnail__item').removeClass('is-hover');
});

 $('.js-list-slide').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
	prevArrow:'.js-slider-prev',
	nextArrow:'.js-slider-next',
    cssEase: 'linear',
	adaptiveHeight: true,
    // autoplay: true,
    autoplaySpeed: 500,
	dots:true,
	accessibility: false,
	responsive:[
		{
			breakpoint: 1024,
			settings: {
				centerMode: true,
				centerPadding: '80px'
			}
		},
		{
			breakpoint: 680,
			settings: {
				centerMode: true,
				centerPadding: '35px'
			}
		}
	]
});
function detailFocus(){
	$('.js-list-slide').off('afterChange').on('afterChange', function(event, slick, currentSlide, nextSlide){
		if ($('body').attr('data-mobile') == 'true'){
			var dataSet = $('.detail-list').offset().top;
			$('html, body').animate({
				scrollTop:dataSet
			},500);
		}
	});
}

$('.js-list-slide').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	var nowNum = nextSlide +1;
	var thumbnailItem = $('.detail-thumbnail__item') ;
	var itemMax = thumbnailItem.length;
	if(nowNum == itemMax ){
		nowNum = 1;
	}
	thumbnailItem.eq(nowNum).addClass('is-active').siblings().removeClass('is-active');
});

$('.js-list-slide').on('swipe', function(event, slick, currentSlide, nextSlide){
	detailFocus();
});

$('.js-thumbnail-move').on('click', function(e){
	e.preventDefault();
	var thumbnailItem = $(this).closest('.detail-thumbnail__item');
	var thumbnailNum = thumbnailItem.index() - 1;
	$('.js-list-slide').slick('slickGoTo', thumbnailNum);
	detailFocus();
});


//스크롤 모션 (real,location)
$(function(){
	$(".js-main-video").each(function(){
		var $this = $(this);
		var n = function() {

			if ($this.offset().top < $(window).scrollTop() + ($(window).height()/1.3) && $this.offset().top > $(window).scrollTop() - $this.height()) {

				$this.addClass('is-active');

				// 윈도우 스크롤 이벤트 함수 n 실행 종료
				// $(window).unbind("scroll", n)
			}else{
				// $this.removeClass('is-active');
			}
		};


		var b = function() {

			// $this 위치값 계산
			if ($this.offset().top < $(window).scrollTop() + ($(window).height()/1.3) && $this.offset().top > $(window).scrollTop() - $this.height()) {

				// 원본 이미지 교체
				$this.addClass('is-active');

				// 윈도우 스크롤 이벤트 함수 n 실행 종료
				// $(window).unbind("scroll", n)
			}else{
				$this.removeClass('is-active');
			}
		};
		// 윈도우 스크롤 이벤트로 함수 n 지속 실행
		$(window).on("scroll", n);
		// $this 위치값 계산
		$(window).on("load", b);
	});
});

var playerCnt = 0;
player = new YKU.Player('youkuplayer', {
	client_id: 'ff5a38840076b667',
	vid: 'XNDE2MDYwNzc4OA',
	 styleid: '0',
	autoplay: true,
	newPlayer: true,
	show_related: true
});

function onPlayEnd(){
	onPlayerStart();
}
function onPlayerStart(){
	player = new YKU.Player('youkuplayer', {
		client_id: 'ff5a38840076b667',
		vid: 'XNDE2MDYwNzc4OA',
		 styleid: '0',
		autoplay: true,
		newPlayer: true,
		show_related: true
	});
	playerCnt += 1;
}

var data;
window.addEventListener('message',function(e){
data = e.data;

switch (data.msg){
		case "onPlayerStart":{
			//console.info(data.msg);
			onPlayerStart();
			break;
		}
		case "onPlayEnd":{
			//console.info(data.msg);/
			if(playerCnt < 2){
				onPlayEnd();
			}
			break;
		}
		/*
		case 'onTimeUpdate':{
			console.info(data.time);
			break;
		}*/
		case "state":{

		var state = data.stateParam;
		//console.info(state);
		break;
		}
	}
},false);

// intro slider
var intro_slide = $(".js-intro-slide");
var intro_slider_bar = $(".intro-slide__bar");
var intro_slide_count = $('.intro-slide__counter');
var intro_slide_timer = 5;
var tick, percentTime;

intro_slide.on("init reInit", function(event, slick, currentSlide, nextSlide){
	var slideNum = (currentSlide ? currentSlide : 0) + 1;
    intro_slide_count.text(slideNum + ' / ' + slick.slideCount);

	$(".intro-slide__item.slick-current").find(".intro-slide__tit").addClass("animated fadeInUp");
	$(".intro-slide__item.slick-current").find(".intro-slide__txt").addClass("animated fadeInUp delay-05s");
});
intro_slide.on("beforeChange", function(event, slick, currentSlide, nextSlide){
	startProgressbar();

	var slideNum = nextSlide + 1;
    intro_slide_count.text(slideNum + ' / ' + slick.slideCount);

	$(".intro-slide__tit").removeClass("animated fadeInUp");
	$(".intro-slide__txt").removeClass("animated fadeInUp delay-05s");
});
intro_slide.on("afterChange", function(event, slick, currentSlide){
	$(".intro-slide__item.slick-current").find(".intro-slide__tit").addClass("animated fadeInUp");
	$(".intro-slide__item.slick-current").find(".intro-slide__txt").addClass("animated fadeInUp delay-05s");
});

intro_slide.slick({
	infinite: true,
	arrows: false,
	dots: false,
	autoplay: true,
	speed: 1000,
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplaySpeed: intro_slide_timer*1000,
	fade: true
});

startProgressbar();

function startProgressbar() {
    resetProgressbar();

    percentTime = 0;
    tick = setInterval(interval, 10);
}

function interval() {
  	percentTime += 1 / (intro_slide_timer+0.1);

  	intro_slider_bar.css({
    	height: percentTime+"%"
  	});

  	if(percentTime >= 100){
      	intro_slide.slick('slickNext');

      	startProgressbar();
    }
}

function resetProgressbar() {
    intro_slider_bar.css({
    	height: 0+'%'
    });

    clearTimeout(tick);
}

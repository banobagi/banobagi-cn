$(document).ready(function(){
	$('.national__link').on('mouseenter',function(){
		$(this).addClass('is-active');
	}).on('mouseleave',function(){
		$(this).removeClass('is-active');
	});

	$('.js-society-link').on('mouseenter',function(){
		$(this).parents('.society__wrap').find('.society__img').addClass('is-active');
	}).on('mouseleave',function(){
		$(this).parents('.society__wrap').find('.society__img').removeClass('is-active');
	});

	$('.select1').each(function(){
		var $this = $(this), numberOfOptions = $(this).children('option').length;

		$this.addClass('select-hidden');
		$this.wrap('<div class="select1__wrap"></div>');
		$this.after('<div class="select-styled"></div>');

		var $styledSelect = $this.next('div.select-styled');
		$styledSelect.text($this.children('option').eq(0).text());

		var $list = $('<ul />', {
		    'class': 'select-options'
		}).insertAfter($styledSelect);

		for (var i = 0; i < numberOfOptions; i++) {
		    $('<li />', {
		        text: $this.children('option').eq(i).text(),
		        rel: $this.children('option').eq(i).val()
		    }).appendTo($list);
		}

		var $listItems = $list.children('li');

		$styledSelect.click(function(e) {
		    e.stopPropagation();
		    $('div.select-styled.active').not(this).each(function(){
		        $(this).removeClass('active').next('ul.select-options').hide();
		    });
		    $(this).toggleClass('active').next('ul.select-options').toggle();
		});

		$listItems.click(function(e) {
		    e.stopPropagation();
		    $styledSelect.text($(this).text()).removeClass('active');
		    $this.val($(this).attr('rel'));
		    $list.hide();
		    //console.log($this.val());
		});

		$(document).click(function() {
		    $styledSelect.removeClass('active');
		    $list.hide();
		});
	});

	var bodyY;
	function scrollStop(){
		bodyY = $(window).scrollTop();
		$('html, body').addClass("no-scroll");
		$('.common').css("top",-bodyY);
	}
	function scrollStart(){
		$('html, body').removeClass("no-scroll");
		$('.common').css('top','auto');
		bodyY = $(window).scrollTop(bodyY);
	}

	//오시는길 문자
	$('.js-sms-close').on('click',function(e){
		e.preventDefault();
		$('.popup-sms, .dim.type-sms').fadeOut();
		scrollStart();
	});
	$('.js-sms-open').on('click',function(e){
		e.preventDefault();
		$('.popup-sms, .dim.type-sms').fadeIn();
		scrollStop();
	});

	// 의료진 상세보기 열기/마우스오버효과
	$('.medical__link').on('mouseenter',function(){
		$(this).parents('.medical__item').addClass('is-active');
	}).on('mouseleave',function(){
		$(this).parents('.medical__item').removeClass('is-active');
	}).on('click',function(e){
		e.preventDefault();
		var docID = $(this).attr('id');
		$('.medical-view__item').removeClass('is-active');
		$('#'+docID+'_view').addClass('is-active');

		TweenMax.to('.medical-view',1,{'top':'0%',ease: Expo.easeInOut});
		TweenMax.to('.medical-view__close',0.5,{opacity:1,rotation:180, delay:1});
		scrollStop();
	});
	// 의료진 상세보기 닫기
	$('.medical-view__close').on('click',function(e){
		e.preventDefault();
		$('body').removeClass('no-scroll');
		TweenMax.to('.medical-view',1,{'top':'100%',ease: Expo.easeInOut});
		TweenMax.to('.medical-view__close',0,{opacity:0,rotation:0, delay:0.8});
		scrollStart();
	});
	$(window).on('load',function(){
		try {
			TweenMax.fromTo('.medical__list', 1, {opacity:0,y:40},{opacity:1,y:0,delay:0.3});
		} catch(error) {}
	});

	// 의료진 이전/다음
	var $docItem = $('.medical-view__item');
	var docTotal = $docItem.length;

	function docReset(){
		$('.tab-v4__item, .tab-v4__content').removeClass('is-active');
		$('.tab-v4__item:first-child, .tab-v4__content:first-of-type').addClass('is-active');
	}
	$('.js-doctor-prev').click(function(e){
		e.preventDefault();
		var prevIndex = $('.medical-view__item.is-active').index() - 1;
		if (prevIndex < 0) {
			prevIndex = docTotal - 1;
		}
		docReset();
		$docItem.removeClass('is-active').eq(prevIndex).addClass('is-active');
	});

	$('.js-doctor-next').click(function(e){
		e.preventDefault();
		var nextIndex = $('.medical-view__item.is-active').index() + 1;
		if (docTotal == nextIndex) {
			nextIndex = 0;
		}
		docReset();
		$docItem.removeClass('is-active').eq(nextIndex).addClass('is-active');
	});
	//의료진소개 약력/저서 스크롤
	$(window).load(function(){
		$('.js-scroll').mCustomScrollbar({
			theme:"minimal-dark"
		});
	});

	//진료절차
	$('.step__slide-list').slick({
	  prevArrow: '.js-step-prev',
	  nextArrow: '.js-step-next',
	  responsive: [
	  {
	    breakpoint: 993,
	    settings: {
	    slidesToShow: 1
	    }
	  },
	  {
	    breakpoint: 681,
	    settings: {
	    slidesToShow: 1
	    }
	  }
	  ]
	});
	var saleTotalNum = $('.step__slide-item').length;
	$('.step__totalpage').html(saleTotalNum-2);
	$('.step__slide-list').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	  $('.step__nowpage').html(nextSlide+1);
	});
	$('.step__slide-list').on('afterChange', function(event, slick, currentSlide){
	  var currentSlide = $('.step__slide-list').slick('slickCurrentSlide');
	  if(currentSlide == '0' || currentSlide == '1' || currentSlide == '2'){
	    $('.step__item').eq(0).addClass('is-active').siblings().removeClass('is-active');
	  }else if(currentSlide == '3' || currentSlide == '4' || currentSlide == '5'  ){
	    $('.step__item').eq(1).addClass('is-active').siblings().removeClass('is-active');
	  }else if(currentSlide == '6' || currentSlide == '7' || currentSlide == '8' || currentSlide == '9' || currentSlide == '10'){
	    $('.step__item').eq(2).addClass('is-active').siblings().removeClass('is-active');
	  }else{
	    $('.step__item').eq(3).addClass('is-active').siblings().removeClass('is-active');
	  }
	});
	$('.js-step[data-slide]').click(function(e){
	  e.preventDefault();
	  var slideno = $(this).data('slide');
	  $('.step__slide-list').slick('slickGoTo',slideno - 1);
	});

	$(".tour-tab__link").on("click", function(e){
		e.preventDefault();
	});

	$(".floor__link").on("click", function(e){
		e.preventDefault();
	});

	// 둘러보기
	$(".floor-slider__list").on("initialized.owl.carousel changed.owl.carousel", function(e){
		if(!e.namespace){
			return;
		}

		var carousel = e.relatedTarget;
		$(".floor-slider__counter").html("<span class='weight-medium'>" + (carousel.relative(carousel.current()) + 1) + '</span>/' + carousel.items().length);
	}).owlCarousel({
		items: 1,
		loop: true,
		dots: false,
		nav: true,
		navContainer: ".floor-slider__nav",
		navText: [$(".js-slider-prev"), $(".js-slider-next")]
	});
});
